#!/bin/bash
# Maintainer: Nico Meisenzahl <nico@meisenzahl.org>
# Not intended for production!

# Disable colors
export BLUEMIX_COLOR=false

# Install & update ibmcloud cli including reqs (kubectl, helm, git, docker cli)
# More details: https://console.bluemix.net/docs/cli/index.html#overview
curl -sL http://ibm.biz/idt-installer | bash

# Login to IBM CLoud
# Create your free account here: https://console.bluemix.net/
echo ''
echo 'Login to IBM CLoud'

ibmcloud login
ibmcloud target --cf